import React, { useContext, useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { registerUserDetails, validateSession } from "../../api";

///// Context /////
import { LoginContext } from "../../context";

import "./Register.css";

const Register = () => {
  const [registerUser, setRegisteruser] = useState({
    email: "",
    name: "",
    password: "",
  });
  const [errorMSG, setErrorMSG] = useState("");
  const { isLoggedIn, setIsLoggedIn } = useContext(LoginContext);

  useEffect(() => {
    const fetchAPI = async () => {
      let res = await validateSession();
      if (res?.data?.success) {
        setIsLoggedIn(true);
      }
      console.log("validate session");
    };

    fetchAPI();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(registerUser);
    let res = await registerUserDetails(registerUser);
    
    if (res?.data?.error) {
      setErrorMSG(res?.data?.error);
    }
    else{
      localStorage.setItem('token', res.data.token);
      setIsLoggedIn(true);
    }
    
  };

  return (
    <div className="register">
      {isLoggedIn ? (
        <Redirect to="/todos" />
      ) : (
        <Form onSubmit={handleSubmit}>
          {errorMSG != "" ? <Form.Text className="text-danger">{errorMSG}</Form.Text> : <></>}
          <Form.Group controlId="registerBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={registerUser.email}
              onChange={(e) =>
                setRegisteruser({ ...registerUser, email: e.target.value })
              }
            />
          </Form.Group>

          <Form.Group controlId="registerBasicName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter name"
              value={registerUser.name}
              onChange={(e) =>
                setRegisteruser({ ...registerUser, name: e.target.value })
              }
            />
          </Form.Group>

          <Form.Group controlId="registerBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={registerUser.password}
              onChange={(e) =>
                setRegisteruser({ ...registerUser, password: e.target.value })
              }
            />
          </Form.Group>

          <Button type="submit">Register</Button>
          <Link className="ml-2" to="/login">
            Login
          </Link>
        </Form>
      )}
    </div>
  );
};

export default Register;
