import React, { useEffect, useState } from "react";
import { Form, Button, Row, Col, Container } from "react-bootstrap";
import {
  addUserTodos,
  deleteTodoItem,
  editTodoItem,
  getUserDetails,
  getUserTodos,
  logoutCurrentUser,
  updateUserDetails,
} from "../../api";

import Cookies from "universal-cookie";
import { Redirect } from "react-router-dom";

const TodoList = () => {
  let newTodoInitialState = {
    title: "",
    taskdate: "",
    taskstatus: "",
  };
  const [list, setList] = useState([]);
  const [newTodo, setNewTodo] = useState(newTodoInitialState);

  const [user, setUser] = useState({});
  const [isAuth, setIsAuth] = useState(true);
  const [errorMSG, setErrorMSG] = useState("");
  const [redirectUrl, setRedirectUrl] = useState("/login");

  useEffect(() => {
    const fetchAPI = async () => {
      // const cookies = new Cookies();
      // console.log("cookies");
      // console.log(cookies);
      let res = await getUserDetails();
      if (res?.data?.success) {
        let todos = await getUserTodos(res.data.userDetails.Email);
        setUser({ ...res.data.userDetails, oldPass: "", newPass: "" });
        setListManually(todos.data);
      } else {
        setIsAuth(false);
        setRedirectUrl(res?.data?.url);
      }
    };
    fetchAPI();
  }, [list.length]);

  const setListManually = (arr) => {
    let newList = arr.map((item) => {
      return {
        ...item,
        editDisabled: true,
      };
    });
    console.log(newList);
    setList(newList);
  };

  const handleAddTodo = async () => {
    let sendObj = {
      ...newTodo,
      email: user.Email,
    };
    console.log(sendObj);
    let res = await addUserTodos(sendObj);
    if (res?.data?.error) {
      console.log("Session exprired");
      setIsAuth(false);
      setRedirectUrl(res?.data?.url);
    } else {
      let todos = await getUserTodos(user.Email);
      setUser({ ...res.data.userDetails, oldPass: "", newPass: "" });
      setListManually(todos.data);
      setNewTodo(newTodoInitialState);
    }
  };

  const handleDelete = async (taskid) => {
    let res = await deleteTodoItem(taskid);
    if (res?.data?.error) {
      console.log("Session exprired");
      setIsAuth(false);
      setRedirectUrl(res?.data?.url);
    } else {
      let todos = await getUserTodos(user.Email);
      setListManually(todos.data);
    }
  };

  const handleSave = async (taskid) => {
    let newItem = list.filter((ele) => ele.taskid == taskid);
    console.log("handleSave");
    console.log(newItem);
    let sendObj = {
      taskid: newItem[0].taskid,
      title: newItem[0].title,
      taskdate: newItem[0].taskdate,
      taskstatus: newItem[0].taskstatus,
    };

    let res = await editTodoItem(sendObj);

    if(res?.data?.error){
      console.log("Session exprired");
      setIsAuth(false);
      setRedirectUrl(res?.data?.url);
    }
    else {
      let todos = await getUserTodos(user.Email);
      setListManually(todos.data);
    }
  };

  const handleUserSave = async () => {
    console.log("editedt user is");
    console.log(user);
    let res = await updateUserDetails(user);
    if (res?.data?.error) {
      console.log("Session exprired");

      if(res?.data?.success == false){
        console.log("password incorect!");
        setErrorMSG(res.data.error);
      }
      setIsAuth(false);
      setRedirectUrl(res?.data?.url);
    }
  };

  const handleUserLogout = async () =>{
    let res = await logoutCurrentUser(user.Email);
    console.log(res);
    if(res?.data?.success){
      setIsAuth(false);
      // setRedirectUrl(res?.data?.url);
    }
  }

  return (
    <>
      {isAuth == true ? (
        <Container className="">
          <Row>
          {errorMSG != "" ? <Form.Text className="text-danger">{errorMSG}</Form.Text> : <></>}
          </Row>
          <Row>
            <Col sm={2}>
              <Form.Control
                type="text"
                placeholder="Enter Name"
                value={user.Username}
                onChange={(e) => {
                  setUser({ ...user, Username: e.target.value });
                }}
              />
            </Col>

            <Col sm={3}>
              <Form.Control
                type="text"
                placeholder="Enter Name"
                value={user.Email}
                disabled={true}
              />
            </Col>

            <Col sm={2}>
              <Form.Control
                type="password"
                placeholder="Old password"
                value={user.oldPass}
                onChange={(e) => {
                  setUser({ ...user, oldPass: e.target.value });
                }}
              />
            </Col>

            <Col sm={2}>
              <Form.Control
                type="password"
                placeholder="New Password"
                value={user.newPass}
                onChange={(e) => {
                  setUser({ ...user, newPass: e.target.value });
                }}
              />
            </Col>

            <Col sm={2}>
              <Button variant="success" onClick={handleUserSave}>
                Save
              </Button>
            </Col>

            <Col sm={2}>
              <Button variant="success" onClick={handleUserLogout}>
                Logout
              </Button>
            </Col>
          </Row>

          <Row className="mt-3">
            <Col sm={3}>Title</Col>

            <Col sm={3}>Date</Col>

            <Col sm={2}>Status</Col>
          </Row>

          <Row className="bg-dark py-2">
            <Col sm={3}>
              <Form.Control
                type="text"
                placeholder="Enter Title"
                value={newTodo.title}
                onChange={(e) =>
                  setNewTodo({ ...newTodo, title: e.target.value })
                }
              />
            </Col>

            <Col sm={3}>
              <Form.Control
                type="date"
                placeholder="Date"
                value={newTodo.taskdate}
                onChange={(e) =>
                  setNewTodo({ ...newTodo, taskdate: e.target.value })
                }
              />
            </Col>

            <Col sm={2}>
              {/* <Form.Control
                type="text"
                placeholder="Status"
                value={newTodo.taskstatus}
                onChange={(e) =>
                  setNewTodo({ ...newTodo, taskstatus: e.target.value })
                }
              /> */}

              <Form.Control
                as="select"
                value={newTodo.taskstatus}
                defaultValue="PENDING"
                onChange={(e) =>
                  setNewTodo({ ...newTodo, taskstatus: e.target.value })
                }
                custom
              >
                <option>PENDING</option>
                <option>WORKING</option>
                <option>DONE</option>
              </Form.Control>
            </Col>

            <Col>
              <Button onClick={handleAddTodo}>Add</Button>
            </Col>
          </Row>

          <Row className="mt-3">
            <Col sm={3}>Title</Col>

            <Col sm={3}>Date</Col>

            <Col sm={2}>Status</Col>
          </Row>
          {list.map((item) => {
            return (
              <Row key={item.taskid} className="bg-dark py-2">
                <Col sm={3}>
                  <Form.Control
                    type="text"
                    placeholder="Enter Title"
                    value={item.title}
                    disabled={item.editDisabled}
                    onChange={(e) => {
                      let newItem = { ...item, title: e.target.value };
                      let newList = list.filter(
                        (ele) => ele.taskid != item.taskid
                      );
                      newList.push(newItem);
                      console.log("new liust is");
                      console.log(newList);
                      setList(newList);
                    }}
                  />
                </Col>

                <Col sm={3}>
                  <Form.Control
                    type="date"
                    placeholder="Date"
                    value={item.taskdate}
                    disabled={item.editDisabled}
                    onChange={(e) => {
                      let newItem = { ...item, taskdate: e.target.value };
                      let newList = list.filter(
                        (ele) => ele.taskid != item.taskid
                      );
                      newList.push(newItem);
                      console.log("new liust is");
                      console.log(newList);
                      setList(newList);
                    }}
                  />
                </Col>

                <Col sm={2}>
                  <Form.Control
                    as="select"
                    value={item.taskstatus}
                    disabled={item.editDisabled}
                    onChange={(e) => {
                      let newItem = { ...item, taskstatus: e.target.value };
                      let newList = list.filter(
                        (ele) => ele.taskid != item.taskid
                      );
                      newList.push(newItem);
                      console.log("new liust is");
                      console.log(newList);
                      setList(newList);
                    }}
                  >
                    <option>PENDING</option>
                    <option>WORKING</option>
                    <option>DONE</option>
                  </Form.Control>
                </Col>

                <Col sm={2}>
                  {item.editDisabled ? (
                    <Button
                      onClick={() => {
                        let newItem = { ...item, editDisabled: false };
                        let newList = list.filter(
                          (ele) => ele.taskid != item.taskid
                        );
                        newList.push(newItem);
                        console.log("new liust is");
                        console.log(newList);
                        setList(newList);
                      }}
                    >
                      Edit
                    </Button>
                  ) : (
                    <Button
                      variant="success"
                      onClick={() => handleSave(item.taskid)}
                    >
                      Save
                    </Button>
                  )}
                </Col>

                <Col sm={2}>
                  <Button
                    variant="danger"
                    onClick={() => handleDelete(item.taskid)}
                  >
                    Delete
                  </Button>
                </Col>
              </Row>
            );
          })}
        </Container>
      ) : (
        <Redirect to={`/login`} />
      )}
    </>
  );
};

export default TodoList;
