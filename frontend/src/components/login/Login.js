import React, { useContext, useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { fetchPerson, validateLogin , validateSession } from "../../api";

///// Context /////
import { LoginContext } from "../../context";

import "./Login.css";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { isLoggedIn, setIsLoggedIn } = useContext(LoginContext);
  const [errorMSG, setErrorMSG] = useState("");

  useEffect(() => {
      const fetchAPI = async () =>{
        let res = await validateSession();
        if(res?.data?.success){
          setIsLoggedIn(true);
        }
        else{
          setIsLoggedIn(false);
        }
        console.log("validate session");
      };

      fetchAPI();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("email", email);
    console.log("password", password);
    let userObj = {
      email: email,
      password:password
    };
    let {data}= await validateLogin(userObj);

    if(data.success){
      localStorage.setItem('token', data.token);
      setIsLoggedIn(true);
      // localStorage.setItem('user', data.user);
    }
    else if(data.error){
      setErrorMSG(data.error)
    }
    

    
  };

  return (
    <div className="login">
      {isLoggedIn ? (
        <Redirect to="/todos" />
      ) : (
        <Form onSubmit={handleSubmit}>
          {errorMSG != "" ? <Form.Text className="text-danger">{errorMSG}</Form.Text> : <></>}
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Button type="submit">Login</Button>

          <Link className="ml-2" to="/register">Register</Link>
        </Form>
      )}
    </div>
  );
};

export default Login;
