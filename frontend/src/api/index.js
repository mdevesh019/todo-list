import axios from "axios";

const url = "http://localhost:5050";

export const fetchPerson = async () => {
  try {
    const response = await axios.get(`${url}/persons`);

    console.log("Person Response is", response);
  } catch (err) {
    console.log(err);
  }
};

export const validateLogin = async (userObj) => {
  try {
    const response = await axios.post(`${url}/auth/login`, userObj);

    console.log("Login Response is", response);

    return response;
  } catch (err) {
    console.log(err);
  }
};

export const registerUserDetails = async (userObj) => {
  try {
      console.log("registerUserDetails" , userObj);
    const response = await axios.post(`${url}/auth/register`, userObj);
    console.log("Register Response is", response);

    return response;
  } catch (err) {
    console.log(err);
  }
};

export const getUserDetails = async () => {
  try {
    console.log("get user details");
    let sessionToken = localStorage.getItem("token");
    console.log(sessionToken);
    const res = await axios.post(`${url}/user`, {token : sessionToken});
    console.log("Get user Response is", res);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const validateSession = async () => {
  try {
    let sessionToken = localStorage.getItem("token");
    const response = await axios.post(`${url}/validate`, {
      token: sessionToken,
    });
    console.log("Get validate Response is", response);
    return response;
  } catch (err) {
    console.log(err);
  }
};

export const getUserTodos = async (email) => {
  try {
    let sessionToken = localStorage.getItem("token");
    const response = await axios.post(`${url}/todos`, {
      token: sessionToken,
      email : email
    });
    console.log("Get todos Response is", response);
    return response;
  } catch (err) {
    console.log(err);
  }
};

export const addUserTodos = async (todo) =>{
    try {
        let reqObj = {
            ...todo,
            token : localStorage.getItem("token")
        }
        // http://localhost:5050/addtodos

        const response = await axios.post(`${url}/addtodos`, reqObj );
        console.log("Add todo response ", response);
        return response;
    } catch (error) {
        console.log(error);
    }
}

export const deleteTodoItem = async (taskid) =>{
    try {
        let reqObj = {
            id:taskid,
            token : localStorage.getItem("token")
        }
        console.log("in delete req ", reqObj);
        const res = await axios.put(`${url}/delete/todos/` , reqObj);
        console.log("delete response" , res);
        return res;
    } catch (error) {
        console.log(error);
    }
}

export const editTodoItem = async (sendObj) =>{
    try {
        let reqObj = {
            ...sendObj,
            token : localStorage.getItem("token")
        };

        console.log("in edit req ", reqObj);
        const res = await axios.put(`${url}/todos/` , reqObj);
        console.log("edit response" , res);
        return res;
    } catch (error) {
        console.log(error);
    }
}

export const updateUserDetails = async (sendObj) =>{
    try {
        let reqObj = {
            ...sendObj,
            token : localStorage.getItem("token")
        };
        console.log("in update user req ", reqObj);
        const res = await axios.put(`${url}/user/` , reqObj);
        console.log("user update response" , res);
        return res;
        
    } catch (error) {
        
    }
}

export const logoutCurrentUser = async (userEmail) =>{
    try {
        console.log("in logoutCurrentUser");
        let reqObj ={email:userEmail};
        const res = await axios.post(`${url}/logout/` , reqObj);

        console.log("logout response is" ,res );
        return res;
    } catch (error) {
        
    }
}