import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Login from "./components/login/Login";
import Home from "./components/home/Home";
import Register from "./components/register/Register";
import { Container, Row } from "react-bootstrap";
import TodoList from "./components/todoList/TodoList";

function App() {
  return (
    <div className="App">
      <Container className=" d-flex justify-content-center">
        <Row className="p-5">
          <Router>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/login" component={Login} />
              <Route path="/register" component={Register} />
              <Route path="/todos" component={TodoList} />
            </Switch>
          </Router>
        </Row>
      </Container>
    </div>
  );
}

export default App;
