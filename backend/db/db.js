const mysql = require("mysql");

const mysqlPool = mysql.createPool({
    connectionLimit : 10,
    host: process.env.LOCAL_DATABASE_HOST,
    user:process.env.LOCAL_DATABASE_USER,
    password:process.env.LOCAL_DATABASE_PASSWORD,
    database:process.env.LOCAL_DATABASE
})

// mysqlConnection.connect((err) =>{
//     if(!err){
//         console.log("database connected");
//     }
//     else{
//         console.log('conection failed'+ JSON.stringify(err));
//     }
// })

module.exports = mysqlPool;