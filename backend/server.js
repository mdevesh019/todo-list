const express = require("express");
const mysql = require("mysql");
const app = express();
const bodyparser = require("body-parser");
const cors = require("cors");

let cookieParser = require("cookie-parser");
const session = require("express-session");

app.use(cookieParser());

app.use(
  session({
    secret: "this is just a secret!",
    resave: false,
    saveUninitialized: false,
  })
);

// const mysqlPool = require('./db/db');
app.use(bodyparser.json());
app.use(cors());

require("dotenv").config();

const PORT = process.env.PORT || 3000;

const mysqlConnection = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

mysqlConnection.connect((err) => {
  if (!err) {
    console.log("database connected");
  } else {
    console.log("conection failed" + JSON.stringify(err));
  }
});

// const authRouter = require('./routes/auth');
// app.use('/auth' , authRouter);

app.listen(PORT, () => {
  console.log(`server started on port ${PORT}`);
});

///////////// middlewares //////////
const checkUserExist = (req, res, next) => {
  console.log("checkUserExist");

  const loginEmail = req.body.email;
  mysqlConnection.query(
    `select email,password, username from Users where email =? `,
    [loginEmail],
    (err, rows, fields) => {
      if (!err) {
        if (rows.length == 0 || req.body.password != rows[0].password) {
          let errorObj = {
            error: "Email / password is incorrect!!",
          };

          // res.status(401);
          res.send(errorObj);
        } else {
          console.log("user exists!!");
          req.locals = {
            userExists: true,
            user: {
              email: rows[0].email,
              username: rows[0].username,
            },
          };
          next();
        }
      } else {
        console.log(err);
      }
    }
  );
};

const checkSessionStatus = (req, res, next) => {
  let errMsg = {
    error: "Unauthorized access",
    url: "/login",
  };

  console.log("checkSessionStatus");
  console.log(req.body);
  if (!req.body.token) {
    // res.status(401);
    res.send(errMsg);
  }
  // if (!req.cookies.todoList) {
  //   res.status(401);
  //   res.send(errMsg);
  // }
  else {
    let s_id = req.body.token;
    mysqlConnection.query(
      `select * from Sessions where SessionID =? `,
      [s_id],
      (err, rows, fields) => {
        if (!err) {
          console.log(rows);
          if (!rows[0].IsActive) {
            console.log("checkSessionStatus is not active");
            // res.status(440);
            res.send({ error: "Session Expired", url: "/login" });
          } else {
            console.log("checkSessionStatus is active");
            next();
          }
        } else {
          console.log(err);
        }
      }
    );
  }
};

const validatePassword = (req, res , next) =>{
  let errMsg = {
    error : "Entered Password is wrong",
    success: false
  }

  mysqlConnection.query(
    `select password from Users where Email =? `,
    [req.body.Email],
    (err, rows, fields) => {
      if (!err) {
        console.log(rows);
        if(rows[0].password != req.body.oldPass){
          res.send(errMsg)
        }
        else{
          next();
        }
      } else {
        console.log(err);
      }
    }
  );
}

const checkRegisteredEmail = (req, res, next) => {
  console.log("checkRegisteredEmail");
  console.log(req.body);
  const loginEmail = req.body.email;
  mysqlConnection.query(
    `select * from Users where Email =? `,
    [loginEmail],
    (err, rows, fields) => {
      if (!err) {
        console.log("rows" , rows.length);
        if (rows.length != 0 ) {
          let errorObj = {
            error: "Email is already registered!!",
          };

          res.send(errorObj);
        } else {
          console.log("next middleware called");
          next();
        }
      } else {
        console.log(err);
      }
    }
  );
};


/////////// utilities ////////
const deAvtivateSession = (loginEmail) => {
  console.log("in deAvtivateSession");
  mysqlConnection.query(
    ` UPDATE Sessions SET IsActive = ? WHERE Email = ? and  IsActive = ?`,
    [false, loginEmail, true],
    (err, rows, fields) => {
      if (!err) {
        console.log("Session deactivated");
      } else {
        console.log(err);
      }
    }
  );
};

const addSession = (email, sid) => {
  mysqlConnection.query(
    `INSERT INTO Sessions SET Email=?, SessionID =?, IsActive= ?`,
    [email, sid, true],
    (err, rows, fields) => {
      if (!err) {
        console.log("Active Session added");
        console.log(rows);
      } else {
        console.log(err);
      }
    }
  );
};


/////////// Auth Routes ////////
app.post("/auth/login", checkUserExist, (req, res) => {
  console.log("Login Request");
  
  console.log(req.locals);
  console.log(req.body);
  if (req.locals.userExists) {
    let loginEmail = req.body.email;
    

    deAvtivateSession(loginEmail);

    addSession(loginEmail, req.session.id);
    // console.log("After session added!!");
    let successMsg = {
      success: true,
      token: req.session.id,
      user: req.locals.user,
    };
    res.cookie("todoList", req.session.id);
    res.status(200);
    res.send(successMsg);
  }

});

app.post("/auth/register", checkRegisteredEmail , (req, res) => {
  console.log("register Request");
  addSession(req.body.email, req.session.id);

  mysqlConnection.query(`INSERT INTO Users SET Email = ?, Username= ?, Password= ?`, [req.body.email,req.body.name, req.body.password], (err, rows) => {
    if (!err) {
      let successMsg = {
        success: true,
        token: req.session.id
      };
      res.cookie("todoList", req.session.id);
      res.status(200);
      res.send(successMsg);
    } else {
      console.log(err);
    }
  });
});

/////////// Todo List Routes ////////
// get all todos //
app.post("/todos", checkSessionStatus, (req, res) => {
  console.log("Get all todos for user");

  // getUserDetails();
  console.log(req.body);
  const email = req.body.email;

  mysqlConnection.query(
    `SELECT taskdate, title, taskstatus, taskid FROM TodoList WHERE Email = ? `,
    [email],
    (err, rows) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// add post //
app.post("/addtodos", checkSessionStatus, (req, res) => {
  console.log("Add task to todo list");

  mysqlConnection.query(` INSERT INTO TodoList SET Email = ? , TaskDate = ?, Title =?, TaskStatus =?  `, [req.body.email, req.body.taskdate, req.body.title,req.body.taskstatus], (err, rows) => {
    if (!err) {
      res.send(rows);
    } else {
      console.log(err);
    }
  });
});

// delete post //
app.put("/delete/todos/", checkSessionStatus, (req, res) => {
  console.log("Deleting Task");
  console.log(req.body);
  const taskid = req.body.id;
  mysqlConnection.query(
    ` Delete from TodoList WHERE taskid = ? `,
    [taskid],
    (err, rows) => {
      if (!err) {
        let successMsg = {
          success : true
        }
        res.send(successMsg);
      } else {
        console.log(err);
      }
    }
  );
});

// edit post //
app.put("/todos", checkSessionStatus, (req, res) => {
  console.log("Updating Task");
  mysqlConnection.query(
    ` UPDATE TodoList SET TaskDate = ?, Title = ? , TaskStatus = ? WHERE taskid = ? `,
    [req.body.taskdate, req.body.title, req.body.taskstatus, req.body.taskid],
    (err, rows) => {
      if (!err) {
        let successMsg = {
          success : true
        }
        res.send(successMsg);
      } else {
        console.log(err);
      }
    }
  );
});

// Validate Session //
app.post("/validate", (req, res) => {
  let errMSg = {
    error: "Session invalid",
    url: "/",
  };

  if (!req.body.token) {
    // res.status(401);
    res.send(errMSg);
  } else {
    let s_id = req.body.token;
    mysqlConnection.query(
      `select * from Sessions where SessionID =? `,
      [s_id],
      (err, rows, fields) => {
        if (!err) {
          console.log(rows);
          if (!rows[0].IsActive) {
            console.log("validate is not active");
            // res.status(440);
            res.send({ error: "Session Expired", url: "/" });
          } else {
            console.log("validate is active");
            res.status(200);
            res.send({ success: true, url: "/todos" });
          }
        } else {
          console.log(err);
        }
      }
    );
  }
});

// Get User details //
app.post("/user", checkSessionStatus, (req, res) => {
  console.log("GEtting user details");
  let s_id = req.body.token;
  mysqlConnection.query(
    ` SELECT Users.Email, Users.Username from Users INNER JOIN Sessions ON Users.Email = Sessions.Email where Sessions.SessionID = ? `,
    [s_id],
    (err, rows) => {
      if (!err) {
        console.log("user details are!" , rows);
        let successMsg = {
          success : true,
          userDetails : rows[0]
        }
        res.send(successMsg);
      } else {
        console.log(err);
      }
    }
  );
});

// update user details //
app.put("/user", checkSessionStatus, validatePassword,  (req, res) => {
  console.log("editing user details");
  let s_id = req.body.token;
  mysqlConnection.query(
    ` UPDATE Users SET Username = ?, Password = ?   WHERE Email = ? `,
    [req.body.Username, req.body.newPass , req.body.Email],
    (err, rows) => {
      if (!err) {
        console.log("user details are!" , rows);
        let successMsg = {
          success : true,
          userDetails : rows[0]
        }
        res.send(successMsg);
      } else {
        console.log(err);
      }
    }
  );
});

app.post("/logout" ,  (req, res)=>{

  console.log("loggin out");
  deAvtivateSession(req.body.email);

  let sendObj ={
    success : true
  }

  res.send(sendObj)
})